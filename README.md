# Redirect

This is a PoC code for *"Type=5, Code=1 (or Lady In The Middle)"* presented
at [Hack in Paris 2016](https://hackinparis.com/talks/) and [BSides Warsaw 2015](http://www.securitybsides.com/w/page/96117554/BSidesWarsaw2015).

To use this software, you need to set up your golang environment first.
Please use https://golang.org/doc/instal instructions to install
(or [Install Go from source](https://golang.org/doc/install/source)).

You can cross compile with Go. See [this blog post by Dave Cheney](http://dave.cheney.net/2015/08/22/cross-compilation-with-go-1-5) for some instructions.
If you need cross compile, installation from source is preferred. 

Next, get gopacket:
```
go get github.com/google/gopacket
```

You may also use go to get this redirect software:
```
go get gitlab.com/litm/redirect
```

To build redirect binary, just use:
```
go build redirect.go
```

To run redirect software, you can either use a compiled binary, or use `go run` command:
```
go run redirect.go [options]
```
